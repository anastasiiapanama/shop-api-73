const User = require("../models/User");

const auth = async (req, res, next) => {
    const token = req.get('Authorization'); // если зарегистрированный полователь ввел правильные данные и token
    // совпадает то он получает доступ к /secret url

    if (!token) {
        return res.status(401).send({error: 'No token present'});
    }

    const user = await User.findOne({token});

    if(!user) {
        return res.status(401).send('Wrong token');
    }

    req.user = user;

    next();
};

module.exports = auth;