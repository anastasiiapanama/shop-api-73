const path = require('path');
const rootPath = __dirname;

const env = process.env.NODE_ENV;

let databaseUrl = 'mongodb://localhost/shop';

let port = 8000;

if (env === 'test') {
    databaseUrl = 'mongodb://localhost/shop_test';
    port = 8010;
}

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public'),
    port,
    db: {
        url: databaseUrl,
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        },
    },
    facebook: {
        appId: '1621345674721871',
        appSecret: '900609a2b12d161717581c301a4d212b',
    },
    google: {
        clientId: '897366196277-uvb9vt4779pverbthcdmkv56h4rrj430.apps.googleusercontent.com'
    }
};