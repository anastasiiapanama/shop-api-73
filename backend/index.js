require('dotenv').config();
const express = require('express');
const cors = require("cors");

// const fileDb = require('./fileDb');
// const mysqlDb = require('./mysqlDb');
// const mongoDb = require('./mongoDb');

const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');

const products = require('./app/products');
const categories = require('./app/categories');
const users = require('./app/users');

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

app.use('/products', products);
app.use('/categories', categories);
app.use('/users', users);

const run = async () => {
    // await fileDb.init();
    // await mysqlDb.connect();
    // await mongoDb.connect();

    await mongoose.connect(config.db.url, config.db.options);

    app.listen(config.port, () => {
        console.log(`Server started on ${config.port} port!`);
    });

    exitHook(async callback => {
        await mongoose.disconnect();
        console.log('mongoose disconnected');
        callback();
    });

    // process.on('exit', () => { // когда сервер будет завершен мы должны отключиться от БД mongodb
    //     mongoDb.disconnect();
    // });
};

run().catch(console.error);