const fs = require('fs').promises;
const express = require('express');
const User = require('../models/User');
const config = require('../config');
const axios = require("axios");
const path = require("path");
const {downloadAvatar} = require("../utils");
const {nanoid} = require("nanoid");
const { OAuth2Client } = require('google-auth-library');
const googleClient = new OAuth2Client(config.google.clientId);
const upload = require('../multer').avatar;

const router = express.Router();

router.post('/', upload.single('avatar'), async (req, res) => {
    try { // регистрация нового пользователя
        const user = new User({
            email: req.body.email,
            password: req.body.password,
            displayName: req.body.displayName,
            avatar: req.file ? req.file.filename : null
        });

        user.generateToken();

        await user.save();
        return res.send(user);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({email: req.body.email}); // login проверка вводимого user и password со стороны клиента

    if (!user) {
        return res.status(401).send({message: 'Email not found'});
    }

    // const isMatch = await bcrypt.compare(req.body.password, user.password); // compare делает сравнение body.password user.password
    const isMatch = await user.checkPassword(req.body.password);

    if(!isMatch) {
        return res.status(401).send({message: 'Password is wrong'});
    }

    user.generateToken();
    
    await user.save();

    return res.send({message: 'Email and password correct!', user});
});

router.delete('/sessions', async (req, res) => {
   const token = req.get('Authorization');
   const success = {message: 'Success'};

   if (!token) {
       return res.send(success);
   }

   const user = await User.findOne({token});

   if (!user) {
       return res.send(success);
   }

   user.generateToken();

   await user.save();

   return res.send(success);
});

router.post('/facebookLogin', async (req, res) => {
    const inputToken = req.body.accessToken; // регистрация через facebook
    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;
    
    try {
        const response = await axios.get(debugTokenUrl);

        if (response.data.data.error) {
            return res.status(401).send({message: 'Facebook token incorrect!'});
        }

        if(response.data.data['user_id'] !== req.body.id) {
            return res.status(401).send({global: 'User ID incorrect'});
        }

        let user = await User.findOne({email: req.body.email})

        if (!user) {
            user = await User.findOne({faceBook: req.body.id});
        }

        const pictureUrl = req.body.picture.data.url;
        const avatarFilename = await downloadAvatar(pictureUrl);

        if(!user) { // если нет и {email: req.body.email} и {faceBook: req.body.id} то тогда создаем нового
            user = new User({
                email: req.body.email || nanoid(),
                password: nanoid(),
                facebookId: req.body.id,
                displayName: req.body.name
            });
        }

        user.avatar = avatarFilename;
        user.generateToken();
        await user.save();

         return res.send({message: 'Success', user});

    } catch (e) {
        console.log(e)
        return res.status(401).send({global: 'Facebook token incorrect!'});
    }
});

router.post('/googleLogin', async (req, res) => {
    try {
        const ticket = await googleClient.verifyIdToken({
            idToken: req.body.tokenId,
            audience: process.env.CLIENT_ID
        });

        console.log(ticket.getPayload());

        const {name, email, sub: ticketUserId} = ticket.getPayload(); // можно взять picture

        if (req.body.googleId !== ticketUserId) {
            return res.status(401).send({global: 'User ID incorrect!'});
        }

        let user = await User.findOne({email});

        if(!user) {
            user = new User({
                email: email,
                password: nanoid(),
                displayName: name
            });
        }

        user.generateToken();
        await user.save();

        return res.send({message: 'Success', user});
    } catch (e) {
        console.log(e)
        return res.status(500).send({global: 'Server error. Please try again'});
    }
});

module.exports = router;