const express = require('express');

// const fileDb = require('../fileDb'); // fs
// const mysqlDb = require('../mysqlDb'); // mysql

// const mongoDb = require('../mongoDb'); // mongodb
// const ObjectId = require('mongodb').ObjectId; // mongodb

const Product = require("../models/Product"); // mongoose
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const upload = require('../multer').products;

const router = express.Router();

router.get('/', async (req, res) => {
    // const products = await fileDb.getItems();
    // const [products] = await mysqlDb.getConnection().query('SELECT * FROM products');

    // const db = mongoDb.getDb();
    // const products = await db.collection('products').find().toArray(); // mongodb

    const criteria = {};

    if(req.query.category) {
        criteria.category = req.query.category;
    }
    
    try {
        const products = await Product.find(criteria).populate('category', 'title'); // mongoose

        res.send(products);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    // const product = await fileDb.getItemById(req.params.id); // fs
    
    // const [products] = await mysqlDb.getConnection().query('SELECT * FROM products WHERE id = ?, [req.params.id]');
    // const product = products[0]; // mysql
    
    // try { // mongo
    //     const db = mongoDb.getDb();
    //
    //     const result = await db.collection('products').findOne({_id: new ObjectId(req.params.id)});
    //
    //     if (result) {
    //         res.send(result)
    //     } else (
    //         res.sendStatus(404)
    //     )
    //
    // } catch (e) {
    //     res.sendStatus(500);
    // }

    try { // mongoose
        const product = await Product.findOne({_id: req.params.id}).populate('category', 'title');

        if (product) {
            res.send(product)
        } else (
            res.sendStatus(404)
        )

    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', auth, permit('admin'), upload.single('image'), async (req, res) => {
    try {
        const productRequestData = {
            category: req.body.category || null,
            title: req.body.title,
            price: req.body.price,
            description: req.body.description,
        }; // productRequestData данные которые пришли из запроса

        if (req.file) {
            productRequestData.image = req.file.filename;
        }

        // await fileDb.addItem(product); // fs

        // const [result] = await mysqlDb.getConnection().query('INSERT INTO products (title, price, description, image) VALUES (?, ?, ?, ?)',
        //     [product.title, product.price, product.description, product.image]);
        // res.send({...product, id: result.insertId}); // mySQL

        // const db = mongoDb.getDb(); // mongodb
        // const result = await db.collection('products').insertOne(product);
        // res.send({...product, _id: result.ops[ 0]});

        const product = new Product(productRequestData);
        await product.save(); // обязательно сохранить товар с вызовом async f save()
        res.send(product);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router; // export routers