const mongoose = require('mongoose');

const bcrypt = require('bcrypt');
const {nanoid} = require("nanoid");

const SALT_WORK_FACTOR = 10; // сколько раз должны захэшировать с новой солью

const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: async function(value) {
                if (this.isModified('email')) {
                    const user = await User.findOne({email: value});
                    return !user;
                }
            },
            message: 'This user is already registered'
        }
    },
    password: {
        type: String,
        required: true
    },
    token: { // временный пароль чтобы привязать его к польз и не запрашивать пароль при действиях на сайте/приложении
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true,
        default: 'user',
        enum: ['user', 'admin']
    },
    facebookId: String,
    displayName: {
        type: String,
        required: true
    },
    avatar: String
});

UserSchema.pre('save', async function (next) { // hook pre перед сохранением будет выполняться функция внутри можно перезаписывать ключи {}
    if (!this.isModified('password')) return next(); // метод который проверяет какое поле было изменено
    // до сохр в базу(при редактировании чтобы не было проблем с перезаписью)

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR); // генерируем соль
    const hash = await bcrypt.hash(this.password, salt); // хэштрование пароля

    this.password = hash;

    next();
});

UserSchema.set('toJSON', { // метод переопределяет {} reqU.body который пришел в запросе в toJSON и удаляет password
    transform: (doc, ret, options) => {
        delete ret.password; // ret - return
        return ret;
    }
});

UserSchema.methods.checkPassword = function(password) {
    return bcrypt.compare(password, this.password); // при login метод сравнивает вводимый паспорт с текущим
};

UserSchema.methods.generateToken = function () { // метод создает token(пароль) авторизованному польз-лю
    // чтобы при повторном входе система его узнавала token нужно генерировать при регистрации и при login перед сохр в базу
    this.token = nanoid();
};

const User = mongoose.model('User', UserSchema);

module.exports = User;