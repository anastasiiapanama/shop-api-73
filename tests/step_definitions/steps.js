const { I } = inject();

Given('я нахожусь на странице регистрации', () => {
  // From "features/auth.feature" {"line":9,"column":5}

  I.amOnPage('/register');
});

When('я ввожу в поля текст:', table => {
  for (const id in table.rows) {
    if (id < 1) {
      continue;
    }

    const cells = table.rows[id].cells;
    const field = cells[0].value;
    const value = cells[1].value;

    I.fillField(field, value);
  }
});

When('нажимаю на кнопку {string}', (name) => {
  I.click(`//button//*[contains(text(),"${name}")]/..`);

  I.wait(10);
});

Then('вижу {string}', () => {
  // From "features/auth.feature" {"line":16,"column":5}
  throw new Error('Not implemented yet');
});