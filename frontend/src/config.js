const env = process.env.REACT_APP_ENV;

let domain = 'localhost:8000';

if (env === 'test') {
    domain = 'localhost:8010';
}

export const apiURL = 'http://' + domain;
export const googleClientId = '897366196277-uvb9vt4779pverbthcdmkv56h4rrj430.apps.googleusercontent.com';
export const facebookAppId = '1621345674721871';